# Fun with Numbers
#
# Mark your solutions with @ex(#) where # is the number of the exercise. See
# the example solution #0 in the code.
#
# Exercise
# ========
# 1. Write a function that adds all integers in a given interval. The interval
#    shall be passed as two arguments to the function with default values 0 and 6.
#
#    Example:
#    --------
#    The sum of 0 to 6 is 21 (0 + 1 + 2 + 3 + 4 + 5 + 6)
# 2. Write a function that can determine if a number is prime. Prime numbers
#    can only be divided by 1 and itself. Examples of prime numbers include 13,
#    17, 19 etc. The function should return True if the argument is prime.
# 3. Write a function that takes an integer argument and returns the words for
#    the digits: 123 becomes "onetwothree" and 5193 becomes "fiveoneninethree".
# 4. Write a function that performs a multiplication by successive addition.
#
#    Example:
#    --------
#    3 * 4 is the same as 3 + 3 + 3 + 3
#    2 * 5 is the same as 2 + 2 + 2 + 2 + 2

from numbers_tests import ex


@ex(0)
def inc(n):
    """Returns n + 1."""
    return n + 1


@ex(1)
def total_sum(start=0, end=6):
    pass

# Write the rest of the functions here.

# You can run the test suite as a script from the command-line with:
# $ python3.3 numbers.py
if __name__ == "__main__":
    ex.test()